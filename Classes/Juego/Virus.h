#pragma once
#include "cocos2d.h"
USING_NS_CC;

class Virus
{

	public:

		Virus(int i);
		~Virus();

		void newResistenteAntibioticos();
		void newInfluenza();
		void newDepresion();
		void newEbola();
		void newPaludismo();

		int getId();
		char* getNombre();
		char* getDescripcion();
		int getCodigoColision();
		Sprite* getSprite();

	private:

		int m_id;
		char *m_nombre;
		char *m_descripcion;
		Sprite *m_sprite;

		int m_codigo_colision;
};
