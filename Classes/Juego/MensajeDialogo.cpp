#include "MensajeDialogo.h"
#include "SimpleAudioEngine.h"
#include "../Engine2D/Definitions.h"
#include <iostream>

USING_NS_CC;

void MensajeDialogo::i_inicializarLabelTexto()
{
	Size visible_size;

	visible_size = Director::getInstance()->getVisibleSize();

	texto_mensaje = "1.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cago en dios.\n2.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cag\n3.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cago en dios.\n4.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cag. 1.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cago en dios.\n2.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cag\n3.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cago en dios.\n4.Buen jaleito que tiene que haber en esta vaina del cocos2d que ahora mismo me esta amargando un poco la vida me cag.";
	//texto_mensaje = "1.Buen jaleito que tiene que haber; 2.Buen jaleito que tiene que haber.";

	label_texto = Label::create();
	label_texto->setString(texto_mensaje);
	label_texto->setWidth(visible_size.width - 2 * margen_label_texto);
	label_texto->setTextColor(Color4B::BLACK);

	item_texto_mensaje = MenuItemLabel::create(label_texto);
	item_texto_mensaje->setDisabledColor(Color3B(255, 255, 255));
	item_texto_mensaje->setEnabled(false);
}

void MensajeDialogo::i_inicializarBoton()
{
	char *texto_boton;
	Label *label_texto_boton;

	texto_boton = "Ok!";

	label_texto_boton = Label::create();
	label_texto_boton->setString(texto_boton);
	label_texto_boton->setTextColor(Color4B::BLACK);

	boton_ok = MenuItemLabel::create(label_texto_boton, CC_CALLBACK_0(MensajeDialogo::i_ocultar, this));
}

void MensajeDialogo::i_ocultar()
{
	if (m_node != NULL)
	{
		scroll_layer->jumpToTop();
		m_node->setVisible(false);
	} 
}

void MensajeDialogo::i_crearMenuItemTextoBoton()
{
	Size visible_size;
	Vec2 origin;

	visible_size = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	menu = Menu::create(item_texto_mensaje, boton_ok, NULL);
	menu->alignItemsVertically();
	menu->setPositionX(visible_size.width / 2);
}

void MensajeDialogo::i_ajustarTamanoScrollable()
{
	Size visible_size;
	float ancho_menu, alto_menu;

	menu->alignItemsVertically();

	visible_size = Director::getInstance()->getVisibleSize();

	ancho_menu = boton_ok->getBoundingBox().size.width;
	alto_menu = boton_ok->getBoundingBox().size.height + item_texto_mensaje->getBoundingBox().size.height;

	if (alto_menu > 50)
	{
		scroll_layer->setContentSize(Size(visible_size.width, 50));
		m_recuadro->setContentSize(Size(visible_size.width, 50 + margen_label_texto));
	}
	else
	{
		scroll_layer->setContentSize(Size(visible_size.width, alto_menu));
		m_recuadro->setContentSize(Size(visible_size.width, alto_menu + margen_label_texto));
	}

	scroll_layer->setInnerContainerSize(Size(ancho_menu, alto_menu));

	scroll_layer->jumpToTop();

	menu->setPositionY(alto_menu / 2);	
}

void MensajeDialogo::i_crearMenuScrollable()
{
	Size visible_size;
	Vec2 origin;
	float y_scroll;
	Vec2 position;

	visible_size = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	scroll_layer = ui::ScrollView::create();
	scroll_layer->setDirection(ui::ScrollView::Direction::VERTICAL);
	scroll_layer->setTouchEnabled(true);
	scroll_layer->setBounceEnabled(false);

	y_scroll = margen_label_texto + origin.y;
	scroll_layer->setPositionY(y_scroll);

	m_recuadro = Sprite::create("RecuadroTexto.png");
	m_recuadro->setAnchorPoint(Vec2(0, 0));
	m_recuadro->setPositionY(y_scroll - margen_label_texto / 2);
	m_node->addChild(m_recuadro, 0);

	scroll_layer->jumpToTop();

	// Liberar
	scroll_layer->retain();

	scroll_layer->requestFocus();

	i_ajustarTamanoScrollable();

	scroll_layer->addChild(menu);
}

// on "init" you need to initialize your instance
MensajeDialogo::MensajeDialogo()
{
	margen_label_texto = PTM_RATIO;

	// Liberar despues
	m_node = Node::create();
	m_node->retain();
	m_node->setVisible(false);

	i_inicializarLabelTexto();
	i_inicializarBoton();
	i_crearMenuItemTextoBoton();
	
	i_crearMenuScrollable();
}

MensajeDialogo::~MensajeDialogo()
{
	//##Implementar
}

Node *MensajeDialogo::getNode()
{
	if (m_node != NULL) 
	{
		m_node->addChild(scroll_layer);
	}
	else
	{
		m_node = Node::create();
	}

	return m_node;
}

void MensajeDialogo::setMensaje(char *nuevo_mensaje)
{
	this->texto_mensaje = nuevo_mensaje;
	item_texto_mensaje->setString(this->texto_mensaje);
	i_ajustarTamanoScrollable();
}

void MensajeDialogo::mostrar()
{
	if (m_node != NULL)
	{
		m_node->setVisible(true);
	}
}


void MensajeDialogo::setEnabled(bool enabled)
{
	menu->setEnabled(enabled);
	scroll_layer->setEnabled(enabled);
}