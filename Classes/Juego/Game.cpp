#include "Game.h"
#include "ui/CocosGUI.h"
#include "../Engine2D/VirtualStickAuto.h"
#include "NPC.h"
#include "PeleaScene.h"
#include "Virus.h"
#include "VirusScene.h"
#include "MainMenu.h"

USING_NS_CC;

Mundo* Mundo::m_instance = NULL;

void Game::establecerIdioma()
{
	UserDefault *user_default;

	user_default = cocos2d::UserDefault::getInstance();

	user_default->setStringForKey(IDIOMA_TAG, ESPANOL);
}


void Game::seguirJugando(cocos2d::Ref *sender)
{
	m_menu_pausa->setVisible(false);
	m_fondo_menu->setVisible(false);

	m_boton_pausa->setEnabled(true);
	mensaje_dialogo->setEnabled(true);

	m_input_virtual->getNode()->resume();
}

void Game::volverMenuPrincipal(cocos2d::Ref *sender)
{
	auto scene = MainMenu::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

void Game::crearMenuPausa()
{
	float tamano_fuente;
	Label *label_reanudar, *label_salir;
	MenuItemLabel *menu_item_reanudar, *menu_item_salir;

	tamano_fuente = 12;

	label_reanudar = Label::createWithTTF("Seguir jugando", FUENTE_TEXTOS, tamano_fuente);
	menu_item_reanudar = MenuItemLabel::create(label_reanudar, CC_CALLBACK_1(Game::seguirJugando, this));

	label_salir = Label::createWithTTF("Volver al menu principal", FUENTE_TEXTOS, tamano_fuente);
	menu_item_salir = MenuItemLabel::create(label_salir, CC_CALLBACK_1(Game::volverMenuPrincipal, this));

	m_menu_pausa = Menu::create(menu_item_reanudar, menu_item_salir, NULL);
	m_menu_pausa->alignItemsVertically();
	m_menu_pausa->setVisible(false);
	addChild(m_menu_pausa, 300);

	m_fondo_menu = Sprite::create("Background/fondoPausa.png");
	m_fondo_menu->setVisible(false);
	m_fondo_menu->setOpacity(128);
	addChild(m_fondo_menu, 200);
}

void Game::mostrarMenuPausa()
{
	m_menu_pausa->setVisible(true);
	m_fondo_menu->setVisible(true);

	m_boton_pausa->setEnabled(false);
	mensaje_dialogo->setEnabled(false);

	m_input_virtual->returnInitialPosition();
	m_input_virtual->getNode()->pause();
}

void Game::anadirBotonPausa()
{
	Size visibleSize;
	Vec2 origin;
	
	float margen;

	crearMenuPausa();

	m_boton_pausa = ui::Button::create("botonPause2.png");
	
	m_boton_pausa->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type) 
	{
		switch (type)
		{
			case ui::Widget::TouchEventType::BEGAN:

				break;

			case ui::Widget::TouchEventType::ENDED:

				mostrarMenuPausa();
				break;

			default:

				break;
		}
	});
	
	m_boton_pausa->setAnchorPoint(Vec2(1, 1));

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	margen = 16.0f;
	m_boton_pausa->setPosition(Vec2(visibleSize.width - margen + origin.x, visibleSize.height - margen + origin.y));

	this->addChild(m_boton_pausa, 100);
}

// on "init" you need to initialize your instance
bool Game::init()
{
	Vec2 origin;

	m_scrollable = Node::create();
	m_scrollable->setPosition(origin.x, origin.y);

	SimpleGame::init();

	Mundo::destroyInstance();
	m_mundo = Mundo::getInstance();
	addGameEntity(m_mundo);

	m_player = PhysicsPlayer::create();
	addGameEntity(m_player);

	mensaje_dialogo = new MensajeDialogo();

	// Controles virtuales
	m_input_virtual = VirtualStickAuto::create();
	m_input_virtual->retain();
	addGameEntity(m_input_virtual);

	establecerIdioma();

	this->preloadResources();
	this->start();

	anadirBotonPausa();

    return true;
}

Game::~Game() 
{
	m_player->release();

	// (d) Descomentar. Liberaci�n de memoria de controles virtuales
	m_input_virtual->release();
	mensaje_dialogo->~MensajeDialogo();
}

void Game::start()
{
	Vec2 origin;

	addEachGameEntityNodeTo(m_scrollable);
	m_scrollable->removeChild(m_input_virtual->getNode());

	origin = Director::getInstance()->getVisibleOrigin();

	m_player->setTransform(Vec2(16 + origin.x, 16 + origin.y), 0.0f);

	addChild(m_scrollable);

	addChild(m_input_virtual->getNode(), 80);
	m_input_virtual->addTouchListeners(this);
	m_input_virtual->addKeyboardListeners(this);

	addChild(mensaje_dialogo->getNode(), 90);
}

void Game::moverMapa()
{
	Size visibleSize;
	Vec2 origin;
	Vec2 position_scrollable, position_player;
	float nueva_position_scrollable_x, nueva_position_scrollable_y;
	Size tile_size, map_size, tamano_total_mapa;
	float limite_scrollable_x, limite_scrollable_y;

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	position_scrollable = m_scrollable->getPosition();
	position_player = m_player->getNode()->getPosition();

	nueva_position_scrollable_x = visibleSize.width / 2 - position_player.x;
	nueva_position_scrollable_y = visibleSize.height / 2 - position_player.y;

	tile_size = m_mundo->getTileSize();
	map_size = m_mundo->getMapSize();
	tamano_total_mapa = Size(tile_size.width * map_size.width, tile_size.height * map_size.height);

	limite_scrollable_x = tamano_total_mapa.width - visibleSize.width;
	limite_scrollable_y = tamano_total_mapa.height - visibleSize.height;

	if (nueva_position_scrollable_x < 0.0 && abs(nueva_position_scrollable_x) < limite_scrollable_x)
	{
		m_scrollable->setPositionX(nueva_position_scrollable_x);
	}

	if (nueva_position_scrollable_y < 0.0 && abs(nueva_position_scrollable_y) < limite_scrollable_y)
	{
		m_scrollable->setPositionY(nueva_position_scrollable_y);
	}
	
	position_scrollable = m_scrollable->getPosition();

	if (abs(position_scrollable.x) > limite_scrollable_x)
	{
		m_scrollable->setPositionX(-limite_scrollable_x);
	}
	if (abs(position_scrollable.y) > limite_scrollable_y)
	{
		m_scrollable->setPositionY(-limite_scrollable_y);
	}
}

void Game::updateEachFrame(float delta)
{
	updateEachGameEntityWithDelta(delta);

	m_player->move(m_input_virtual->getAxis(Axis::AXIS_HORIZONTAL), m_input_virtual->getAxis(Axis::AXIS_VERTICAL), delta);

	moverMapa();

	playerColisionaConLetreros();
	playerColisionaConNPC();
	playerColisionaConVirus();
}

void Game::preloadResources() 
{
	preloadEachGameEntity();
}

void Game::playerColisionaConLetreros()
{
	bool colisiona;
	Letrero *letrero;

	colisiona = m_player->colisionaConLetreros();

	letrero = m_player->letrero_colisionado;

	if (colisiona == true && letrero!= NULL && letrero->getMostrar() == true)
	{
		mostrarDialogo();
	}
}

void Game::playerColisionaConNPC()
{
	bool colisiona;
	NPC *npc;

	colisiona = m_player->colisionaConNPC();

	npc = m_player->npc_colisionado;

	if (colisiona == true && npc != NULL)
	{
		PeleaScene *pelea_scene;
		int salud_moral_player;

		pelea_scene = PeleaScene::createScene();

		salud_moral_player = m_player->getSaludMoral();
		pelea_scene->inicializarDatos(npc, salud_moral_player);

		Director::getInstance()->pushScene(TransitionFade::create(TRANSITION_TIME, pelea_scene));

		m_input_virtual->returnInitialPosition();
	}
}

void Game::playerColisionaConVirus()
{
	bool colisiona;
	Virus *virus;

	colisiona = m_player->colisionaConVirus();

	virus = m_player->virus_colisionado;

	if (colisiona == true && virus != NULL)
	{
		VirusScene *virus_scene;

		virus_scene = VirusScene::createScene();
		virus_scene->inicializarDatos(virus);

		Director::getInstance()->pushScene(TransitionFade::create(TRANSITION_TIME, virus_scene));

		m_input_virtual->returnInitialPosition();
	}
}

void Game::mostrarDialogo()
{	
	if (m_player->letrero_colisionado != NULL)
	{
		mensaje_dialogo->setMensaje(m_player->letrero_colisionado->getMensaje());
		mensaje_dialogo->mostrar();
	}
}