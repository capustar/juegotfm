#pragma once


#include "../Engine2D/GameEntity.h"

class Bug : public GameEntity
{
	public:

		bool init();

		Node* getNode();
		void update(float delta);

		void move();

		char* getNombre();
		char* getDescripcion();

		void addTouchListeners(cocos2d::Node *node);

	private:

		int m_id;
		char *m_nombre;
		char *m_descripcion;
		Sprite *m_sprite;

		bool touchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
};