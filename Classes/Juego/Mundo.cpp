//
//  Mundo.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "Mundo.h"
#include "iostream"
#include "ui/CocosGUI.h"
#include "../Engine2D/Definitions.h"
#include "NPC.h"
#include "Virus.h"
#include "tinyxml2.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


using namespace std;
//Mundo *Mundo::m_instance;

bool Mundo::init()
{
    GameEntity::init();

    m_tiledMapHelper = TiledMapHelper::create();
    m_tiledMapHelper->retain();
    
    // Inicialización del mundo físico
    m_world = new b2World(b2Vec2(0, 0));
    
    m_debugDraw = new CocosDebugDraw(PTM_RATIO);
    m_world->SetDebugDraw(m_debugDraw);
    
    uint32 flags = 0;
    //flags += b2Draw::e_shapeBit;
    //flags += b2Draw::e_jointBit;
    flags += b2Draw::e_aabbBit;
    //flags += b2Draw::e_pairBit;
    //flags += b2Draw::e_centerOfMassBit;
    
    //m_debugDraw->SetFlags(flags);
    
    return true;
}

void Mundo::destroyInstance() {
    if(m_instance!=NULL) {
        m_instance->release();
        m_instance=NULL;
    }
}

Mundo* Mundo::getInstance()
{
    if(Mundo::m_instance==NULL) 
	{
        Mundo::m_instance = Mundo::create();
        //Hago un retain hasta el final de los tiempos, soy un singleton
        m_instance->retain();
    }
    
    return Mundo::m_instance;
}

Mundo::~Mundo(){
    CCLOG("Mundo es Singleton, no debería morir hasta el final. Cuidado...");
    m_tiledMapHelper->release();
    
    if(m_world != NULL) {
        delete m_world;
        m_world = NULL;
    }
    if(m_debugDraw != NULL) {
        delete m_debugDraw;
        m_debugDraw = NULL;
    }
    
	/*
    for (auto npc: this->m_npcs)
    {
        npc->release();
    }
	*/
}

void Mundo::inicializarNPC()
{
	TMXObjectGroup *grupo_mapa_npc = m_tiledMapHelper->getTiledMap()->getObjectGroup("NPC");

	auto objetos_mapa_letreros = grupo_mapa_npc->getObjects();

	int numero_npc;

	numero_npc = 0;

	for (cocos2d::Value object : objetos_mapa_letreros)
	{
		b2FixtureDef fixtureDef;
		ValueVector polyline = object.asValueMap().at("polylinePoints").asValueVector();
		//Calcula las x e y en funcion de la posicion del mapa. La posicion del mapa sera su (0,0)
		float x = object.asValueMap().at("x").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionX();
		float y = object.asValueMap().at("y").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionY();

		NPC *npc;
		string id_string;
		int id;

		//Genera cadena en box2d
		int numPoints = (int)polyline.size();
		b2Vec2 *chainPoints = new b2Vec2[numPoints];

		int i = 0;
		for (auto point : polyline)
		{
			chainPoints[i++].Set(
				(x + point.asValueMap().at("x").asFloat()) / PTM_RATIO,
				(y - point.asValueMap().at("y").asFloat()) / PTM_RATIO);
		}

		b2ChainShape chain;
		chain.CreateChain(chainPoints, numPoints);
		fixtureDef.shape = &chain;

		id_string = object.asValueMap().at("id").asString();

		id = std::stoi(id_string);

		npc = new NPC(id);

		fixtureDef.userData = npc;

		m_body->CreateFixture(&fixtureDef);

		numero_npc++;
	}
}

void Mundo::string_copy(const char *from, char *to)
{
	while ((*to++ = *from++) != '\0')
		;
}

void Mundo::inicializarLetreros()
{	
	TMXObjectGroup *grupo_mapa_letreros = m_tiledMapHelper->getTiledMap()->getObjectGroup("Letreros");

	auto objetos_mapa_letreros = grupo_mapa_letreros->getObjects();

	int contador;

	for (auto objeto : objetos_mapa_letreros)
	{
		//objects.push_back(object_physic_mensaje);
		std::string id = objeto.asValueMap().at("id").asString();
		//log("id mensaje is %s", id);
	}

	contador = 0;

	for (cocos2d::Value object : objetos_mapa_letreros)
	{
		b2FixtureDef fixtureDef;
		ValueVector polyline = object.asValueMap().at("polylinePoints").asValueVector();
		//Calcula las x e y en funcion de la posicion del mapa. La posicion del mapa sera su (0,0)
		float x = object.asValueMap().at("x").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionX();
		float y = object.asValueMap().at("y").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionY();

		Letrero *letrero;
		string id_letrero;
		string id_texto_letrero, mensaje_letrero;
		int id_texto_letrero_int;
		char *texto_letrero_char;
		char *id_texto_letrero_char;

		//Genera cadena en box2d
		int numPoints = (int)polyline.size();
		b2Vec2 *chainPoints = new b2Vec2[numPoints];

		int i = 0;
		for (auto point : polyline)
		{
			chainPoints[i++].Set(
				(x + point.asValueMap().at("x").asFloat()) / PTM_RATIO,
				(y - point.asValueMap().at("y").asFloat()) / PTM_RATIO);
		}

		b2ChainShape chain;
		chain.CreateChain(chainPoints, numPoints);
		fixtureDef.shape = &chain;

		id_letrero = object.asValueMap().at("id").asString();

		id_texto_letrero_int = contador;

		if ((m_mapa == 1 && std::stoi(id_letrero) == 44) || (m_mapa == 2 && std::stoi(id_letrero) == 37))
		{
			id_texto_letrero_char = "LetreroPrincipal";

			texto_letrero_char = m_textos->getTexto(id_texto_letrero_char);

			letrero = new Letrero(id_texto_letrero_int, texto_letrero_char);
		}
		else
		{
			id_texto_letrero = "Letreroid" + std::to_string(contador);

			id_texto_letrero_char = new char[strlen(id_texto_letrero.c_str()) + 1];

			string_copy(id_texto_letrero.c_str(), id_texto_letrero_char);

			texto_letrero_char = m_textos->getTexto(id_texto_letrero_char);

			letrero = new Letrero(id_texto_letrero_int, texto_letrero_char);
		}

		fixtureDef.userData = letrero;

		m_body->CreateFixture(&fixtureDef);

		contador++;
	}	
}

void Mundo::inicializarVirus()
{
	TMXObjectGroup *grupo_mapa_npc = m_tiledMapHelper->getTiledMap()->getObjectGroup("Virus");

	auto objetos_mapa_letreros = grupo_mapa_npc->getObjects();

	int numero_npc;

	numero_npc = 0;

	for (cocos2d::Value object : objetos_mapa_letreros)
	{
		b2FixtureDef fixtureDef;
		ValueVector polyline = object.asValueMap().at("polylinePoints").asValueVector();
		//Calcula las x e y en funcion de la posicion del mapa. La posicion del mapa sera su (0,0)
		float x = object.asValueMap().at("x").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionX();
		float y = object.asValueMap().at("y").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionY();

		Virus *virus;

		//Genera cadena en box2d
		int numPoints = (int)polyline.size();
		b2Vec2 *chainPoints = new b2Vec2[numPoints];

		int i = 0;
		for (auto point : polyline)
		{
			chainPoints[i++].Set(
				(x + point.asValueMap().at("x").asFloat()) / PTM_RATIO,
				(y - point.asValueMap().at("y").asFloat()) / PTM_RATIO);
		}

		b2ChainShape chain;
		chain.CreateChain(chainPoints, numPoints);
		fixtureDef.shape = &chain;

		virus = new Virus(numero_npc);

		fixtureDef.userData = virus;

		m_body->CreateFixture(&fixtureDef);

		numero_npc++;
	}
}

void Mundo::initPhysics() 
{
    b2BodyDef bodyDef;
    bodyDef.position.Set(0, 0);
    bodyDef.type = b2BodyType::b2_staticBody;
    
    m_body = m_world->CreateBody(&bodyDef);
    
    b2FixtureDef fixtureDef;
    
    TMXObjectGroup *groupPhysics = m_tiledMapHelper->getTiledMap()->getObjectGroup("Fisica");
    
    auto objects = groupPhysics->getObjects();
    
    for(auto object : objects) 
	{
        ValueVector polyline = object.asValueMap().at("polylinePoints").asValueVector();
		//Calcula las x e y en funcion de la posicion del mapa. La posicion del mapa sera su (0,0)
        float x = object.asValueMap().at("x").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionX();
        float y = object.asValueMap().at("y").asFloat() + m_tiledMapHelper->getTiledMap()->getPositionY();
        
        //Genera cadena en box2d
        int numPoints = (int)polyline.size();
        b2Vec2 *chainPoints = new b2Vec2[numPoints];
        
        int i=0;
        for(auto point: polyline) 
		{
            chainPoints[i++].Set(
							(x + point.asValueMap().at("x").asFloat()) / PTM_RATIO,
                            (y - point.asValueMap().at("y").asFloat()) / PTM_RATIO);
        }
        
        b2ChainShape chain;
        chain.CreateChain(chainPoints, numPoints);
        fixtureDef.shape = &chain;
        
        m_body->CreateFixture(&fixtureDef);
    }
}

void Mundo::reestablecerMapaInicial()
{
	UserDefault *user_default;
	int mapa_inicial;

	user_default = cocos2d::UserDefault::getInstance();

	mapa_inicial = 1;
	user_default->setIntegerForKey(MAPA_TAG, mapa_inicial);
}

void Mundo::preloadResources()
{
	UserDefault *user_default;

	user_default = cocos2d::UserDefault::getInstance();
	m_mapa = user_default->getIntegerForKey(MAPA_TAG);
	//mapa = 2;

	switch (m_mapa)
	{
		case 1:

			this->m_nombre_mapa = "Ashland2/mapaAshland.tmx";
			this->m_nombre_fichero_textos = "Ashland2/textosAshland.xml";
			break;

		case 2:

			this->m_nombre_mapa = "DarkDimension2/mapaDarkDimension.tmx";
			this->m_nombre_fichero_textos = "DarkDimension2/textosDarkDimension.xml";
			break;

		default:

			this->m_nombre_mapa = "Ashland/mapa50x50.tmx";
			this->m_nombre_fichero_textos = "Ashland/mapa50x50textos.xml";
			break;
	}

	m_tiledMapHelper->loadTileMap(this->m_nombre_mapa, "Fisica", "Mensajes");

	//Liberar en el destructor
	m_textos = new Textos(this->m_nombre_fichero_textos);

	initPhysics();
	inicializarLetreros();
	

	switch (m_mapa)
	{
		case 1:

			inicializarNPC();
			break;

		case 2:

			inicializarVirus();
			break;

		default:

			break;
	}
}

Node* Mundo::getNode(){
    
    Node *node = Node::create();

	/*
    //Añado los NPCS en las posiciones que corresponda
    for (auto npc: this->m_npcs){
        Node *n = npc->getNode();
        node->addChild(n);
    }
	*/
    
    //Añado el tiledmap
    node->addChild(m_tiledMapHelper->getTiledMap());
    
    //Añado nodo debug de fisicas
    node->addChild(m_debugDraw->GetNode(),9999);
    
    return node;
    
}

int Mundo::getTileGIDAtPosition(const Point &tileCoords){
    return m_tiledMapHelper->getTileGIDAtPosition(tileCoords);
}

Point Mundo::tileCoordForPosition(const Vec2& position){
   return m_tiledMapHelper->tileCoordForPosition(position);
}

Rect Mundo::tileRectForPosition(const cocos2d::Vec2 &position) {
    Point tileCoords = m_tiledMapHelper->tileCoordForPosition(position);
    return m_tiledMapHelper->rectForTileAt(tileCoords);
}

void Mundo::deleteTileAt(const Point &tileCoords){
    auto sprite = m_tiledMapHelper->getCollisionLayer()->getTileAt(tileCoords);
    
    //Labda para borrar el tile cuando termine la animación
    //& copia por referencia
    //= copia por valor
    //Cuidado, pq tileCoords estara liberado cuando se ejecute lambda
    //Por defecto [=]
    auto actionDelete = CallFunc::create([&,tileCoords](){m_tiledMapHelper->deleteTileAt(tileCoords);});
    
    auto actionAnim = Spawn::create(MoveBy::create(.1, Vec2(0,5)),FadeOut::create(1),RotateBy::create(1,90),NULL);
    auto action = Sequence::create(actionAnim,actionDelete,NULL);
    
    sprite->runAction(action);

}

Size Mundo::getTileSize()
{
    return m_tiledMapHelper->getTiledMap()->getTileSize();
}


Size Mundo::getMapSize()
{
	return m_tiledMapHelper->getTiledMap()->getMapSize();
}

/*
Vector<NPC*> Mundo::getNPCs(){
    return m_npcs;
}
*/

void Mundo::update(float delta)
{
    // Actualiza físicas
    m_debugDraw->Clear();
    m_world->DrawDebugData();
    
    m_world->ClearForces();
    m_world->Step(1.0f/60.0f, 6, 2);
    
	/*
    for (auto npc: this->m_npcs){
        npc->update(delta);
    }
	*/
}