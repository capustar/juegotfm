#include "VirusScene.h"
#include "../Engine2D/cocos_pop_transition.hpp"
#include "../Engine2D/Definitions.h"
#include <ctime>

USING_NS_CC;

using namespace std;

//Scene* VirusScene::createScene()
VirusScene* VirusScene::createScene()
{
	VirusScene *pelea;

	pelea = VirusScene::create();

	return pelea;
}

void VirusScene::inicializarDatos(Virus *virus)
{
	this->m_virus = virus;
}

void VirusScene::moverJugador(Touch* toque, Event* evento)
{
	auto posicionToque = toque->getLocation();
	if (_conejo->getBoundingBox().containsPoint(posicionToque)) {
		_conejo->setPositionX(posicionToque.x);
	}
}

void VirusScene::inicializarFisica(cocos2d::Sprite* sprite, int collisionBitmask)
{
	auto cuerpo = PhysicsBody::createCircle(sprite->getContentSize().width / 2);
	cuerpo->setCollisionBitmask(collisionBitmask);
	cuerpo->setContactTestBitmask(true);
	cuerpo->setDynamic(true);
	sprite->setPhysicsBody(cuerpo);
}

void VirusScene::agregarBombar(float dt)
{
	auto director = Director::getInstance();
	auto tamano = director->getWinSize();
	Sprite* bomba = nullptr;
	for (int i = 0; i < 2; i++)
	{
		//bomba = m_virus->getSprite();
		bomba = Sprite::create(m_virus->getSprite()->getResourceName());
		bomba->setAnchorPoint(Vec2::ZERO);
		bomba->setPosition(CCRANDOM_0_1() * tamano.width, tamano.height);
		inicializarFisica(bomba, m_collision_bitmask_bomba);
		bomba->getPhysicsBody()->setVelocity(Vect(0, ((CCRANDOM_0_1() + 0.2f) * -250)));
		_bombas.pushBack(bomba);
		this->addChild(bomba, 1);
	}
}

void VirusScene::inicializarToque()
{
	auto escuchador = EventListenerTouchOneByOne::create();
	escuchador->onTouchBegan = [](Touch* touch, Event* event) { return true; };
	escuchador->onTouchMoved = CC_CALLBACK_2(VirusScene::moverJugador, this);
	escuchador->onTouchEnded = [=](Touch* touch, Event* event) {};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(escuchador, this);
}

void VirusScene::pantallaSuperada(float dt)
{
	m_virus_superado = true;
	this->scheduleOnce(schedule_selector(VirusScene::volverPantallaAnterior), 5);
}

void VirusScene::update(float dt)
{
	if (m_virus_superado == false)
		agregarBombar(dt);
}

// on "init" you need to initialize your instance
bool VirusScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	this->initWithPhysics();
	this->getPhysicsWorld()->setGravity(Vect(0, 0));
	this->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	auto director = Director::getInstance();
	auto tamano = director->getWinSize();

	auto spriteFondo = Sprite::create("Virus/fondo.png");
	spriteFondo->setAnchorPoint(Vec2::ZERO);
	spriteFondo->setPosition(0, 0);
	//addChild(spriteFondo, 0);

	_conejo = Sprite::create("Virus/corazon.png");
	_conejo->setPosition(tamano.width / 2, tamano.height * 0.23);
	m_collision_bitmask_conejo = 1;
	inicializarFisica(_conejo, m_collision_bitmask_conejo);
	addChild(_conejo, 1);

	m_virus_superado = false;

	inicializarToque();

	m_collision_bitmask_bomba = 2;

	schedule(schedule_selector(VirusScene::update), 3.0f);

	EventListenerPhysicsContact *contact_listener = EventListenerPhysicsContact::create();

	contact_listener->onContactBegin = CC_CALLBACK_1(VirusScene::detectarColision, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contact_listener, this);

	this->scheduleOnce(schedule_selector(VirusScene::pantallaSuperada), 10);

	return true;
}

void VirusScene::volverPantallaAnterior(float dt)
{
	cocos2d::Director::getInstance()->pushScene
	(
		pop_scene_with<cocos2d::TransitionFade>::create(TRANSITION_TIME)
	);
}

bool VirusScene::detectarColision(cocos2d::PhysicsContact &contact)
{
	PhysicsBody *body_a, *body_b;

	body_a = contact.getShapeA()->getBody();
	body_b = contact.getShapeB()->getBody();

	if (m_virus_superado == true)
	{
		return false;
	}

	if ((body_a->getCollisionBitmask() == m_collision_bitmask_bomba && body_b->getCollisionBitmask() == m_collision_bitmask_conejo)
		|| (body_a->getCollisionBitmask() == m_collision_bitmask_conejo && body_b->getCollisionBitmask() == m_collision_bitmask_bomba))
	{
		_conejo->setTexture("Virus/corazonRoto.png");
		this->scheduleOnce(schedule_selector(VirusScene::volverPantallaAnterior), DISPLAY_TIME_SPLASH_SCENE);

		return true;
	}

    if ((body_a->getCollisionBitmask() == m_collision_bitmask_bomba && body_b->getCollisionBitmask() == m_collision_bitmask_bomba)
        || (body_a->getCollisionBitmask() == m_collision_bitmask_bomba && body_b->getCollisionBitmask() == m_collision_bitmask_bomba))
    {
        return true;
    }

	return false;
}

void VirusScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	return;
#endif

	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
