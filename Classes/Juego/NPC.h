#pragma once

#include "../Engine2D/GameEntity.h"
#include <iostream>
#include "Ataque.h"
#include <vector>

class NPC
{

	public:

		NPC(int i);
		~NPC();

		void initEjemplo();

		bool atacar(int dano);
		Ataque *defenderse();

		bool permitirHuir();

		std::vector<Ataque*> getAccionesAtaque();
		int getSaludMoral();
		Sprite *getSprite();

		char *getNombre();

		int getCodigoColision();

		void establecerValoresIniciales();

		void newEsbirroPatriarcado();
		void newEntrepeneur();
		void newBurgues();
		void newAntivacunas();
		void newAntiEcologista();

	private:

		int m_id;
		char *nombre;
		char *descripcion;
		int salud_moral;
		Sprite *m_sprite;
		std::vector<Ataque*> acciones_ataque;
		std::vector<Ataque*> acciones_defensa;
		bool permitir_huir;
		bool derrotado;
		int m_codigo_colision;
		
		void inicializarAccionesAtaque();
		void inicializarAccionesDefensa();
};
