#ifndef __MENSAJEDIALOGO_H__
#define __MENSAJEDIALOGO_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class MensajeDialogo
{

	public:

		MensajeDialogo();
		~MensajeDialogo();

		virtual Node* getNode();

		void mostrar();

		void setMensaje(char *nuevo_mensaje);

		void setEnabled(bool enabled);

		//CREATE_FUNC(MensajeDialogo);

	private:

		char *texto_mensaje;

		Label *label_texto;
		cocos2d::MenuItemLabel *item_texto_mensaje;
		cocos2d::MenuItemLabel *boton_ok;
		cocos2d::Menu *menu;

		cocos2d::ui::ScrollView* scroll_layer;

		Sprite *m_recuadro;

		Node *m_node;

		float margen_label_texto;

		void i_ocultar();
		void i_inicializarLabelTexto();
		void i_inicializarBoton();
		void i_crearMenuItemTextoBoton();
		void i_ajustarTamanoScrollable();
		void i_crearMenuScrollable();

		
};

#endif // __MENSAJEDIALOGO_H__
