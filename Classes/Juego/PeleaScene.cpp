#include "PeleaScene.h"
#include "NPC.h"
#include <vector>
#include "../Engine2D/cocos_pop_transition.hpp"
#include "Game.h"
#include "Mundo.h"
#include "../Engine2D/Definitions.h"
#include "GameOverScene.h"

USING_NS_CC;

using namespace std;

PeleaScene* PeleaScene::createScene()
{
	PeleaScene *pelea;

	pelea = PeleaScene::create();
	
	return pelea;
}

void PeleaScene::actualizarGUI()
{
	int salud_moral_rival;

	salud_moral_rival = m_npc->getSaludMoral();

	m_gui_vida_NPC->setString(StringUtils::format("Moral rival: %d", salud_moral_rival));
	m_gui_vida_player->setString(StringUtils::format("Moral propia: %d", this->salud_moral_player));
}

void PeleaScene::inicializarGUI()
{
	Size visible_size;
	Vec2 origin;

	visible_size = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	m_gui_vida_NPC = Label::createWithTTF("Moral rival: 100", FUENTE_TEXTOS, m_tamano_fuente);
	m_gui_vida_NPC->setAnchorPoint(Vec2(0.5, 1));
	m_gui_vida_NPC->setPosition(Vec2(origin.x + visible_size.width / 4, origin.y + visible_size.height - m_margen));

	m_gui_vida_player = Label::createWithTTF("Moral propia: 100", FUENTE_TEXTOS, m_tamano_fuente);
	m_gui_vida_player->setAnchorPoint(Vec2(0.5, 1));
	m_gui_vida_player->setPosition(Vec2(visible_size.width - visible_size.width / 4, origin.y + visible_size.height - m_margen));

	actualizarGUI();

	addChild(m_gui_vida_NPC);
	addChild(m_gui_vida_player);
}

void PeleaScene::cargarNuevoMapa()
{
	UserDefault *user_default;
	int nuevo_mapa;
	Scene *scene;

	user_default = cocos2d::UserDefault::getInstance();

	nuevo_mapa = 2;
	user_default->setIntegerForKey(MAPA_TAG, nuevo_mapa);

	scene = Game::create();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

void PeleaScene::cargarGameOver()
{
	Scene *scene;

	scene = GameOverScene::create();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}
void PeleaScene::actualizarColorGUIPlayerNormal(float df)
{
	m_gui_vida_player->setTextColor(Color4B::WHITE);
	m_menu_ataques->setEnabled(true);
}

void PeleaScene::actualizarGUIPlayerDano()
{
	m_gui_vida_player->setString(StringUtils::format("Moral propia: %d", this->salud_moral_player));
	m_gui_vida_player->setTextColor(Color4B::RED);
	this->scheduleOnce(CC_SCHEDULE_SELECTOR(PeleaScene::actualizarColorGUIPlayerNormal), 1.0f);
}

void PeleaScene::actualizarDefensaNPC(Ataque *ataque_defensa)
{
	string descripcion_ataque;

	descripcion_ataque = string(m_npc->getNombre()) + string(" ha usado: ") + string(ataque_defensa->getNombre());

	m_defensa_npc->setString(descripcion_ataque);
	m_defensa_npc->setVisible(true);
}

void PeleaScene::defensaNPC(float df)
{
	Ataque *ataque_defensa;
	string descripcion_ataque;

	ataque_defensa = m_npc->defenderse();

	salud_moral_player = salud_moral_player - ataque_defensa->getDano();

	if (salud_moral_player <= 0)
	{
		CCLOG("GAME OVER");
		salud_moral_player = 0;
		cargarGameOver();
	}

	actualizarGUIPlayerDano();
	actualizarDefensaNPC(ataque_defensa);
	cocos2d::Device::vibrate(0.1);
}

void PeleaScene::actualizarColorGUIRivalNormal(float df)
{
	m_gui_vida_NPC->setTextColor(Color4B::WHITE);
}

void PeleaScene::actualizarGUIRivalDano()
{
	int salud_moral_rival;

	salud_moral_rival = m_npc->getSaludMoral();

	m_gui_vida_NPC->setString(StringUtils::format("Moral rival: %d", salud_moral_rival));
	m_gui_vida_NPC->setTextColor(Color4B::RED);
	this->scheduleOnce(CC_SCHEDULE_SELECTOR(PeleaScene::actualizarColorGUIRivalNormal), 0.5f);
}

void PeleaScene::i_atacar(cocos2d::Ref *sender)
{
	std::vector<Ataque*> acciones_ataque;

	acciones_ataque = m_npc->getAccionesAtaque();

	auto opcion = (MenuItemLabel*)sender;

	int posicion_elemento_seleccionado = opcion->getLocalZOrder();

	Ataque *ataque_seleccionado = acciones_ataque.at(posicion_elemento_seleccionado);

	if (m_npc->atacar(ataque_seleccionado->getDano()) == true)
	{
		this->scheduleOnce(CC_SCHEDULE_SELECTOR(PeleaScene::defensaNPC), 1.0f);
	}
	else
	{
		cargarNuevoMapa();
	}

	actualizarGUIRivalDano();
	m_menu_ataques->setEnabled(false);
	m_defensa_npc->setVisible(false);
}

void PeleaScene::crearMenuAtaques()
{
	vector<Ataque*> ataques;
	int i, num_ataques;
	Vector<MenuItem*> items_menu_ataques;
	float alto_items_menu;
	Size visible_size;
	Vec2 origin;
	float y_menu;

	m_menu->setVisible(false);

	ataques = m_npc->getAccionesAtaque();

	i = 0;
	num_ataques = ataques.size();
	alto_items_menu = 0;

	visible_size = Director::getInstance()->getVisibleSize();

	for (i = 0; i < num_ataques; i++)
	{
		Label *label_atacar;

		label_atacar = Label::createWithTTF(ataques.at(i)->nombre, FUENTE_TEXTOS, m_tamano_fuente);
		label_atacar->setWidth(visible_size.width / 2.25);
		label_atacar->setAlignment(TextHAlignment::CENTER);
		items_menu_ataques.pushBack(MenuItemLabel::create(label_atacar, CC_CALLBACK_1(PeleaScene::i_atacar, this)));

		alto_items_menu += label_atacar->getContentSize().height;
	}

	m_menu_ataques = Menu::create();
	m_menu_ataques->initWithArray(items_menu_ataques);
	m_menu_ataques->alignItemsVertically();

	origin = Director::getInstance()->getVisibleOrigin();

	y_menu = origin.y + (alto_items_menu / 2) + 16;
	//menu->setPositionY(y_menu);
	m_menu_ataques->setPositionX(origin.x + visible_size.width - visible_size.width / 4);
	addChild(m_menu_ataques);

	inicializarGUI();
}

void PeleaScene::i_huir()
{
	log("huir");
	if (m_npc->permitirHuir() == true)
	{
		cocos2d::Director::getInstance()->pushScene
		(
			pop_scene_with<cocos2d::TransitionFade>::create(TRANSITION_TIME)
		);
	}
}

Menu* PeleaScene::crearMenu()
{
	Menu *menu;
	Label *label_atacar, *label_huir;
	MenuItemLabel *opcion_atacar, *opcion_huir;
	Size visible_size;
	Vec2 origin;
	float x_menu;

	label_atacar = Label::createWithTTF("Discutir", FUENTE_TEXTOS, m_tamano_fuente);
	opcion_atacar = MenuItemLabel::create(label_atacar, CC_CALLBACK_0(PeleaScene::crearMenuAtaques, this));

	label_huir = Label::createWithTTF("Huir", FUENTE_TEXTOS, m_tamano_fuente);
	opcion_huir = MenuItemLabel::create(label_huir, CC_CALLBACK_0(PeleaScene::i_huir, this));

	menu = Menu::create(opcion_atacar, opcion_huir, NULL, NULL);
	menu->alignItemsVertically();

	visible_size = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	x_menu = origin.x + visible_size.width - visible_size.width / 4;
	menu->setPositionX(x_menu);

	return menu;
}

void PeleaScene::anadirSprite()
{
	Sprite *sprite_npc;
	Size visible_size;
	Vec2 origin;
	float escala;
	float ancho, alto;
	float x, y;

	sprite_npc = m_npc->getSprite();

	visible_size = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	escala = (visible_size.height / 2.25) / (sprite_npc->getContentSize().height);

	sprite_npc->setContentSize(Size(sprite_npc->getContentSize().width * escala, sprite_npc->getContentSize().height * escala));
	sprite_npc->setAnchorPoint(Vec2(0.5, 0.5));

	x = origin.x + visible_size.width / 4;
	y = origin.y + visible_size.height / 2;
	sprite_npc->setPosition(x, y);

	this->addChild(sprite_npc);
}

void PeleaScene::anadirNombreNPC()
{
	Size visible_size;
	Vec2 origin;

	visible_size = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	m_defensa_npc = Label::createWithTTF(m_npc->getNombre(), FUENTE_TEXTOS, m_tamano_fuente);
	m_defensa_npc->setAnchorPoint(Vec2(0.5, 0));
	m_defensa_npc->setWidth(visible_size.width / 2.25);
	m_defensa_npc->setAlignment(TextHAlignment::CENTER);
	m_defensa_npc->setPosition(origin.x + visible_size.width / 4, origin.y + m_margen);
	m_defensa_npc->setVisible(true);

	addChild(m_defensa_npc);
}

void PeleaScene::inicializarDatos(NPC *npc, int salud_moral_player)
{
	this->m_npc = npc;
	this->salud_moral_player = salud_moral_player;

	this->m_margen = 16;
	this->m_tamano_fuente = 15;

	m_menu = crearMenu();
	this->addChild(m_menu, 1);

	anadirSprite();
	anadirNombreNPC();
}

bool PeleaScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	return true;
}