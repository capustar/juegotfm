#pragma once

class Letrero
{

public:

	Letrero(int id);
	Letrero(int id, char* mensaje);

	int getId();
	char* getMensaje();

	void setMostrar(bool mostrar);
	bool getMostrar();

	int getCodigoColision();

private:

	int id;
	char* mensaje;
	bool mostrar;
	int m_codigo_colision;
};
