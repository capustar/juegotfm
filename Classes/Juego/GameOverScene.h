#ifndef __GAMEOVER_SCENE_H__
#define __GAMEOVER_SCENE_H__

#include "cocos2d.h"
#include "NPC.h"
class GameOverScene : public cocos2d::Scene
{
	public:
		static cocos2d::Scene* createScene();

		virtual bool init();

		CREATE_FUNC(GameOverScene);

	private:
		void GoToMainMenuScene(float dt);
};

#endif // __PELEASCENE_SCENE_H__