#pragma once

#include <vector>
#include "cocos2d.h"
USING_NS_CC;

struct Texto {
	const char *nombre;
	int id;

	char* contenido;
};

class Textos
{
	public:
		Textos(char *nombre_fichero);

		char* getTexto(char *nombre);

	private:

		Data m_data_archivo;
		
		char *m_nombre_fichero;
		std::string m_idioma_juego;

		std::vector<Texto> m_letreros;

		void string_copy(const char *from, char *to);

		void leerFichero();

		void getIdiomaJuego();
};