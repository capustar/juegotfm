class Ataque
{
public:
	char *nombre;
	int dano;

	Ataque();
	Ataque(char *nombre, int dano);

	int getDano();
	char *getNombre();
};