#ifndef __PELEASCENE_SCENE_H__
#define __PELEASCENE_SCENE_H__

#include "cocos2d.h"
#include "NPC.h"

class PeleaScene : public cocos2d::Scene
{
	public:
		static PeleaScene* createScene();
		virtual bool init();
		// implement the "static create()" method manually
		CREATE_FUNC(PeleaScene);

		void inicializarDatos(NPC *npc, int salud_moral_player);

	private:

		NPC *m_npc;
		int salud_moral_player;

		Menu *m_menu;
		Menu *m_menu_ataques;

		Label *m_gui_vida_NPC;
		Label *m_gui_vida_player;

		Label *m_defensa_npc;

		float m_margen;
		float m_tamano_fuente;

		void anadirSprite();
		void anadirNombreNPC();

		cocos2d::Menu* crearMenu();

		void crearMenuAtaques();

		void actualizarGUI();
		void actualizarColorGUIPlayerNormal(float df);
		void actualizarGUIPlayerDano();
		void actualizarDefensaNPC(Ataque *ataque_defensa);
		void actualizarColorGUIRivalNormal(float df);
		void actualizarGUIRivalDano();
		void inicializarGUI();

		void cargarNuevoMapa();
		void cargarGameOver();

		void defensaNPC(float df);
		void i_atacar(cocos2d::Ref *sender);
		void i_huir();
};

#endif // __PELEASCENE_SCENE_H__
