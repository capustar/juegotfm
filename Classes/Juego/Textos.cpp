#include "Textos.h"
#include <iostream>
#include "tinyxml2.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../Engine2D/Definitions.h";

using namespace std;

void Textos::string_copy(const char *from, char *to)
{
	while ((*to++ = *from++) != '\0')
		;
}

void Textos::leerFichero()
{
	std::string ruta_archivo;
	tinyxml2::XMLDocument xml_doc;
	tinyxml2::XMLError eResult;
	tinyxml2::XMLNode* root_map;
	tinyxml2::XMLElement *element_letreros, *element_mensajes;

	ruta_archivo = FileUtils::getInstance()->fullPathForFilename(this->m_nombre_fichero);
	m_data_archivo = FileUtils::getInstance()->getDataFromFile(ruta_archivo);

	eResult = xml_doc.Parse((const char*)m_data_archivo.getBytes(), m_data_archivo.getSize());
	CCASSERT(eResult == tinyxml2::XML_SUCCESS, "Error leyendo el fichero xml");

	root_map = xml_doc.FirstChildElement("map");
	CCASSERT(root_map != nullptr, "Error leyendo el fichero xml");

	element_mensajes = root_map->FirstChildElement();

	while (element_mensajes != nullptr)
	{
		tinyxml2::XMLElement *element_mensaje;
		const char *id_const;
		char *id;
		char *mensaje;
		Texto texto;

		id_const = element_mensajes->Name();

		element_mensaje = element_mensajes->FirstChildElement();

		while (element_mensaje != nullptr)
		{
			const char *idioma;

			idioma = element_mensaje->Name();

			if ((string)idioma == this->m_idioma_juego)
			{
				const char *mensaje_const;

				mensaje_const = element_mensaje->GetText();

				mensaje = new char[strlen(mensaje_const) + 1];

				string_copy(mensaje_const, mensaje);

				break;
			}

			element_mensaje = element_mensaje->NextSiblingElement();
		}

		id = new char[strlen(id_const) + 1];

		string_copy(id_const, id);

		texto = { id, 1, mensaje };

		m_letreros.push_back(texto);

		element_mensajes = element_mensajes->NextSiblingElement();
	}
}

char* Textos::getTexto(char *nombre)
{
	char *contenido_texto;
	int i;
	Texto texto;

	for (i = 0; i < m_letreros.size(); i++)
	{
		texto = m_letreros.at(i);

		if (strcmp(texto.nombre, nombre) == 0)
		{
			contenido_texto = texto.contenido;
			break;
		}
	}

	return contenido_texto;
}

void Textos::getIdiomaJuego()
{
	UserDefault *user_default;

	user_default = cocos2d::UserDefault::getInstance();
	this->m_idioma_juego = user_default->getStringForKey(IDIOMA_TAG);
}

Textos::Textos(char *nombre_fichero)
{
	this->m_nombre_fichero = nombre_fichero;

	getIdiomaJuego();
	leerFichero();
}