#ifndef __VIRUSSCENE_SCENE_H__
#define __VIRUSSCENE_SCENE_H__

#include "cocos2d.h"
#include "Virus.h"

class VirusScene : public cocos2d::Scene
{
	public:
		// there's no 'id' in cpp, so we recommend returning the class instance pointer
		//static cocos2d::Scene* createScene();
		static VirusScene* createScene();

		// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
		virtual bool init();

		// a selector callback
		void menuCloseCallback(cocos2d::Ref* pSender);

		void inicializarDatos(Virus *virus);

		// implement the "static create()" method manually
		CREATE_FUNC(VirusScene);

	private:
		
		Virus *m_virus;

		bool m_virus_superado;

		unsigned m_tiempo_inicio, m_tiempo_actual;

		cocos2d::Sprite* _conejo;
		int m_collision_bitmask_conejo;
		cocos2d::Vector<cocos2d::Sprite*> _bombas;
		int m_collision_bitmask_bomba;

		void inicializarToque();
		void pantallaSuperada(float dt);
		void agregarBombar(float dt);
		void inicializarFisica(cocos2d::Sprite* sprite, int collisionBitmask);
		void moverJugador(cocos2d::Touch* toque, cocos2d::Event* event);

		void volverPantallaAnterior(float dt);

		bool detectarColision(cocos2d::PhysicsContact &contact);
		void update(float dt);


};

#endif // __VIRUSSCENE_SCENE_H__