#include "Letrero.h"
#include "../Engine2D/Definitions.h"

Letrero::Letrero(int id)
{
	this->id = id;
	this->mensaje = "";
	this->mostrar = false;
	this->m_codigo_colision = CODIGO_COLISION_LETRERO;
}

Letrero::Letrero(int id, char* mensaje)
{
	this->id = id;
	this->mensaje = mensaje;
	this->mostrar = false;
	this->m_codigo_colision = CODIGO_COLISION_LETRERO;
}

int Letrero::getId()
{
	return this->id;
}

char* Letrero::getMensaje()
{
	return this->mensaje;
}

void Letrero::setMostrar(bool mostrado)
{
	this->mostrar = mostrado;
}

bool Letrero::getMostrar()
{
	return this->mostrar;
}

int Letrero::getCodigoColision()
{
	return m_codigo_colision;
}