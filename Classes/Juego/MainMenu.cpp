/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "MainMenu.h"
#include "SimpleAudioEngine.h"
#include "../Engine2D/Definitions.h"
#include "ui/CocosGUI.h"
#include "Game.h"
#include "VirusScene.h"

USING_NS_CC;

Scene* MainMenu::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainMenu::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in MainMenuScene.cpp\n");
}

bool MainMenu::anadirOpcionReanudarJuego()
{
	bool anadir;
	UserDefault *user_default;
	int mapa;

	anadir = false;

	user_default = cocos2d::UserDefault::getInstance();
	mapa = user_default->getIntegerForKey(MAPA_TAG);

	if (mapa >= 2 && mapa <= 3)
	{
		anadir = true;
	}

	return anadir;
}

void MainMenu::reanudarJuego(cocos2d::Ref* pSender)
{
	auto *scene = Game::create();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}

// on "init" you need to initialize your instance
bool MainMenu::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
	
    Size visible_size = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto coord_x_mitad = visible_size.width / 2;
	auto coord_y_mitad = visible_size.height / 2;

	Sprite *background_sprite = Sprite::create("Background/postapocalypse2.png");
	background_sprite->setPosition(Point(visible_size.width / 2 + origin.x, visible_size.height / 2 + origin.y));
	background_sprite->setContentSize(Size(visible_size.width, visible_size.height));
	this->addChild(background_sprite);

	Vector<MenuItem*> items_menu;

	bool anadir_opcion_reanudar = anadirOpcionReanudarJuego();
	if (anadir_opcion_reanudar == true)
	{
		auto label_reanudar = Label::createWithTTF("REANUDAR", "fonts/Marker Felt.ttf", 24);
		auto opcion_reanudar = MenuItemLabel::create(label_reanudar, CC_CALLBACK_1(MainMenu::reanudarJuego, this));
		items_menu.pushBack(opcion_reanudar);
	}

	auto label_jugar = Label::createWithTTF("NUEVA PARTIDA", "fonts/Marker Felt.ttf", 24);
	auto opcion_jugar = MenuItemLabel::create(label_jugar, CC_CALLBACK_1(MainMenu::empezarJuego, this));
	items_menu.pushBack(opcion_jugar);

	auto label_salir = Label::createWithTTF("SALIR", "fonts/Marker Felt.ttf", 24);
	auto opcion_salir = MenuItemLabel::create(label_salir, CC_CALLBACK_1(MainMenu::menuCloseCallback, this));
	items_menu.pushBack(opcion_salir);

	auto menu = Menu::create();
	menu->initWithArray(items_menu);
	menu->alignItemsVertically();

	this->addChild(menu, 1);

    return true;
}


void MainMenu::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
}

void MainMenu::establecerMapaInicial()
{
	UserDefault *user_default;
	int mapa_inicial;

	user_default = cocos2d::UserDefault::getInstance();

	mapa_inicial = 1;
	user_default->setIntegerForKey(MAPA_TAG, mapa_inicial);
}

void MainMenu::empezarJuego(Ref* pSender)
{	
	establecerMapaInicial();
	auto *scene = Game::create();
	//auto scene = VirusScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}
