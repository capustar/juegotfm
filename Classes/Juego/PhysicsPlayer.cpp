#include "PhysicsPlayer.h"
#include "Mundo.h"

bool PhysicsPlayer::init() 
{
	GameEntity::init();

	m_playerSprite = Sprite::create();
	m_playerSprite->retain();

	salud_moral = 100;

	return true;
}

void PhysicsPlayer::preloadResources() 
{
	//Cache de sprites
	auto spriteFrameCache = SpriteFrameCache::getInstance();
	//Cache de animaciones
	auto animCache = AnimationCache::getInstance();

	Animation *animacion_andar_derecha, *animacion_andar_izquierda, *animacion_andar_arriba, *animacion_andar_abajo;

	//Si no estaba el spritesheet en la cach� lo cargo
	//if (!spriteFrameCache->getSpriteFrameByName("run-1.png")) 
	if (!spriteFrameCache->getSpriteFrameByName("derecha1.png")) 
	{
		spriteFrameCache->addSpriteFramesWithFile("Personajes/Bob/bob.plist");
	}

	m_body = NULL;

	Animate *actionAnimate;
	RepeatForever* actionRepeat;

	animacion_andar_derecha = Animation::create();
	animacion_andar_derecha->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("derecha1.png"));
	animacion_andar_derecha->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("derecha2.png"));
	animacion_andar_derecha->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("derecha3.png"));
	animacion_andar_derecha->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("derecha4.png"));
	animacion_andar_derecha->setDelayPerUnit(0.1);
	animCache->addAnimation(animacion_andar_derecha, "andar_derecha");
	actionAnimate = Animate::create(animacion_andar_derecha);
	actionRepeat = RepeatForever::create(actionAnimate);
	m_action_andar_derecha = Speed::create(actionRepeat, 0.3f);

	animacion_andar_izquierda = Animation::create();
	animacion_andar_izquierda->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("izquierda1.png"));
	animacion_andar_izquierda->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("izquierda2.png"));
	animacion_andar_izquierda->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("izquierda3.png"));
	animacion_andar_izquierda->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("izquierda4.png"));
	animacion_andar_izquierda->setDelayPerUnit(0.1);
	animCache->addAnimation(animacion_andar_izquierda, "andar_izquierda");
	actionAnimate = Animate::create(animacion_andar_izquierda);
	actionRepeat = RepeatForever::create(actionAnimate);
	m_action_andar_izquierda = Speed::create(actionRepeat, 0.3f);

	animacion_andar_arriba = Animation::create();
	animacion_andar_arriba->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("arriba1.png"));
	animacion_andar_arriba->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("arriba2.png"));
	animacion_andar_arriba->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("arriba3.png"));
	animacion_andar_arriba->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("arriba4.png"));
	animacion_andar_arriba->setDelayPerUnit(0.1);
	animCache->addAnimation(animacion_andar_arriba, "andar_arriba");
	actionAnimate = Animate::create(animacion_andar_arriba);
	actionRepeat = RepeatForever::create(actionAnimate);
	m_action_andar_arriba = Speed::create(actionRepeat, 0.3f);

	animacion_andar_abajo = Animation::create();
	animacion_andar_abajo->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("abajo1.png"));
	animacion_andar_abajo->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("abajo2.png"));
	animacion_andar_abajo->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("abajo3.png"));
	animacion_andar_abajo->addSpriteFrame(spriteFrameCache->getSpriteFrameByName("abajo4.png"));
	animacion_andar_abajo->setDelayPerUnit(0.1);
	animCache->addAnimation(animacion_andar_abajo, "andar_abajo");
	actionAnimate = Animate::create(animacion_andar_abajo);
	actionRepeat = RepeatForever::create(actionAnimate);
	m_action_andar_abajo = Speed::create(actionRepeat, 0.3f);
}

Node* PhysicsPlayer::getNode() 
{
	if (m_node == NULL) 
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		m_playerSprite = Sprite::createWithSpriteFrameName("derecha1.png");
		m_playerSprite->setVisible(true);
		m_playerSprite_izquierda = Sprite::createWithSpriteFrameName("izquierda1.png");
		m_playerSprite_izquierda->setVisible(false);
		m_playerSprite_arriba = Sprite::createWithSpriteFrameName("arriba1.png");
		m_playerSprite_arriba->setVisible(false);
		m_playerSprite_abajo = Sprite::createWithSpriteFrameName("abajo1.png");
		m_playerSprite_abajo->setVisible(false);
		Size nuevo_size_sprite = Size(PTM_RATIO * 1, PTM_RATIO * 1);
		//m_playerSprite->setContentSize(nuevo_size_sprite);

		b2World *world = Mundo::getInstance()->getPhysicsWorld();

		b2BodyDef bodyDef;
		bodyDef.type = b2BodyType::b2_dynamicBody;
		bodyDef.fixedRotation = true;

		b2PolygonShape shapeBoundingBox;
		//0.4 para reducir el tama�o del box
		shapeBoundingBox.SetAsBox(
			m_playerSprite->getContentSize().width * 0.4 / PTM_RATIO,
			m_playerSprite->getContentSize().height * 0.4 / PTM_RATIO);

		
		m_body = world->CreateBody(&bodyDef);
		b2Fixture *fixture = m_body->CreateFixture(&shapeBoundingBox, 1.0);
		fixture->SetFriction(0.0f);
		
		m_playerSprite->runAction(m_action_andar_derecha);
		m_playerSprite_izquierda->runAction(m_action_andar_izquierda);
		m_playerSprite_arriba->runAction(m_action_andar_arriba);
		m_playerSprite_abajo->runAction(m_action_andar_abajo);

		m_node = Node::create();
		m_node->addChild(m_playerSprite);
		m_node->addChild(m_playerSprite_izquierda);
		m_node->addChild(m_playerSprite_arriba);
		m_node->addChild(m_playerSprite_abajo);
		m_node->setLocalZOrder(1);
	}

	return m_node;
}

void PhysicsPlayer::setSaludMoral(int nueva_salud_moral)
{
	this->salud_moral = nueva_salud_moral;
}

int PhysicsPlayer::getSaludMoral()
{
	return salud_moral;
}

void PhysicsPlayer::actualizarAnimacion()
{
	if (m_horizontalAxis > 0.1 && m_horizontalAxis > m_verticalAxis)
	{
		m_playerSprite->setVisible(true);
		m_playerSprite_izquierda->setVisible(false);
		m_playerSprite_abajo->setVisible(false);
		m_playerSprite_arriba->setVisible(false);

		m_action_andar_derecha->setSpeed(fabsf(m_horizontalAxis));
	}
	else if(m_horizontalAxis >= 0 && m_horizontalAxis <= 0.1 && m_horizontalAxis >= m_verticalAxis)
	{
		m_action_andar_derecha->setSpeed(0.0f);
		m_playerSprite->setSpriteFrame("derecha1.png");
	}

	if (m_verticalAxis > 0.1 && m_verticalAxis > m_horizontalAxis)
	{
		m_playerSprite->setVisible(false);
		m_playerSprite_izquierda->setVisible(false);
		m_playerSprite_abajo->setVisible(false);
		m_playerSprite_arriba->setVisible(true);

		m_action_andar_arriba->setSpeed(fabsf(m_verticalAxis));
	}
	else if (m_verticalAxis > 0 && m_verticalAxis <= 0.1 && m_verticalAxis >= m_horizontalAxis)
	{
		m_action_andar_arriba->setSpeed(0.0f);
		m_playerSprite->setSpriteFrame("arriba1.png");
	}

	if (m_horizontalAxis < -0.1 && fabsf(m_horizontalAxis) > fabsf(m_verticalAxis))
	{
		m_playerSprite->setVisible(false);
		m_playerSprite_izquierda->setVisible(true);
		m_playerSprite_abajo->setVisible(false);
		m_playerSprite_arriba->setVisible(false);

		m_action_andar_izquierda->setSpeed(fabsf(m_horizontalAxis));
	}
	else if (m_horizontalAxis <= 0 && m_horizontalAxis >= -0.1 && fabsf(m_horizontalAxis) >= fabsf(m_verticalAxis))
	{
		m_action_andar_izquierda->setSpeed(0.0f);
		m_playerSprite_izquierda->setSpriteFrame("izquierda1.png");
	}	

	if (m_verticalAxis < -0.1 && fabs(m_verticalAxis) > fabs(m_horizontalAxis))
	{
		m_playerSprite->setVisible(false);
		m_playerSprite_izquierda->setVisible(false);
		m_playerSprite_abajo->setVisible(true);
		m_playerSprite_arriba->setVisible(false);

		m_action_andar_abajo->setSpeed(fabsf(m_verticalAxis));
	}
	else if (m_verticalAxis <= 0 && m_verticalAxis >= -0.1 && fabs(m_verticalAxis) >= fabs(m_horizontalAxis))
	{
		m_action_andar_abajo->setSpeed(0.0f);
		m_playerSprite_abajo->setSpriteFrame("abajo1.png");
	}

	if (m_action_andar_arriba->getSpeed() > 0 && m_verticalAxis == 0 && m_horizontalAxis == 0)
	{
		m_action_andar_arriba->setSpeed(0.0f);
		m_playerSprite->setSpriteFrame("arriba1.png");
	}
}

void PhysicsPlayer::update(float delta) 
{
	PhysicsGameEntity::update(delta);
	
	m_body->SetLinearVelocity(b2Vec2(
		m_horizontalAxis * 80 / PTM_RATIO,
		m_verticalAxis * 80 / PTM_RATIO));

	actualizarAnimacion();	
}

void PhysicsPlayer::move(float horizontalAxis, float verticalAxis, float delta)
{
	m_horizontalAxis = horizontalAxis;
	m_verticalAxis = verticalAxis;
}

bool PhysicsPlayer::colisionaConLetreros()
{
	b2ContactEdge *edge;

	edge = m_body->GetContactList();

	while (edge != NULL) 
	{
		if (edge->contact->IsTouching()) 
		{
			b2Fixture *fixture;
			Letrero *letrero;

			fixture = edge->contact->GetFixtureA();
			letrero = (Letrero*) fixture->GetUserData();

			if (letrero != NULL && letrero->getCodigoColision() == CODIGO_COLISION_LETRERO)
			{
				if (letrero_colisionado != NULL)
				{
					letrero_colisionado->setMostrar(false);
				}
				else
				{
					letrero_colisionado = letrero;
					letrero_colisionado->setMostrar(true);
				}

				return true;
			}
		}

		edge = edge->next;
	}

	letrero_colisionado = NULL;

	return false;
}

bool PhysicsPlayer::colisionaConNPC()
{
	b2ContactEdge *edge;

	edge = m_body->GetContactList();

	while (edge != NULL)
	{
		if (edge->contact->IsTouching())
		{
			b2Fixture *fixture;
			NPC *npc;

			fixture = edge->contact->GetFixtureA();
			npc = (NPC*)fixture->GetUserData();

			if (npc != NULL 
				&& npc->getCodigoColision() == CODIGO_COLISION_NPC)
			{
				if (npc_colisionado == NULL)
				{
					npc_colisionado = npc;

					return true;
				}
				else
				{
					return false;
				}
			}
		}

		edge = edge->next;
	}

	npc_colisionado = NULL;

	return false;
}

bool PhysicsPlayer::colisionaConVirus()
{
	b2ContactEdge *edge;

	edge = m_body->GetContactList();

	while (edge != NULL)
	{
		if (edge->contact->IsTouching())
		{
			b2Fixture *fixture;
			Virus *virus;

			fixture = edge->contact->GetFixtureA();
			virus = (Virus*)fixture->GetUserData();

			if (virus != NULL
				&& virus->getCodigoColision() == CODIGO_COLISION_VIRUS)
			{
				if (virus_colisionado == NULL)
				{
					virus_colisionado = virus;

					return true;
				}
				else
				{
					return false;
				}
			}
		}

		edge = edge->next;
	}

	virus_colisionado = NULL;

	return false;
}