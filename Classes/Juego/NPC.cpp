#include "Npc.h"
#include <iostream>
#include <string>

using namespace std;

NPC::NPC(int i)
{
	this->permitir_huir = true;
	this->salud_moral = 100;
	this->derrotado = false;

	this->m_codigo_colision = CODIGO_COLISION_NPC;

	switch (i)
	{
		case 33:

			newEsbirroPatriarcado();
			break;

		case 34:

			newEntrepeneur();
			break;

		case 31:

			newBurgues();
			break;

		case 30:

			newAntivacunas();
			break;

		case 32:

			newAntiEcologista();
			break;

		default:

			newBurgues();
			break;
	}
}

NPC::~NPC()
{
	m_sprite->release();
}

void NPC::initEjemplo()
{
	m_id = 0;

	nombre = "Feminista enfadada";
	descripcion = "Feminista ya no busca igualdad, si no venganza";


	inicializarAccionesAtaque();
	inicializarAccionesDefensa();

	

	derrotado = false;

	m_sprite = Sprite::create("Sprites/feminista.png");
	m_sprite->retain();
}

void NPC::inicializarAccionesAtaque()
{
	acciones_ataque.push_back(new Ataque("Mansplaining", 30));
	acciones_ataque.push_back(new Ataque("Manspreading", 10));
	acciones_ataque.push_back(new Ataque("No escuchar", 25));
	acciones_ataque.push_back(new Ataque("Matizar su discurso, Matizar su discurso, Matizar su discurso, atizar su discurso, Matizar su discurso, Matizar su discurso", 15));
	acciones_ataque.push_back(new Ataque("Micromachismo", 15));
}

void NPC::inicializarAccionesDefensa()
{
	acciones_defensa.push_back(new Ataque("Sororidad", 20));
	acciones_defensa.push_back(new Ataque("Empoderamiento", 30));
	acciones_defensa.push_back(new Ataque("Consigna feminista", 5));
	acciones_defensa.push_back(new Ataque("Dato sobre violencia de genero", 10));
	acciones_defensa.push_back(new Ataque("Ante la duda tu la viuda", 15));
}

void NPC::establecerValoresIniciales()
{
	this->permitir_huir = true;
	this->salud_moral = 100;
	this->derrotado = false;

	this->m_codigo_colision = CODIGO_COLISION_NPC;
}

void NPC::newEsbirroPatriarcado()
{
	m_id = 0;

	nombre = "Esbirro del patriarcado";
	descripcion = "Hombre que quiere perpetuar el patriarcado";

	m_sprite = Sprite::create("Minijuego1/esbirro.png");
	m_sprite->retain();

	acciones_ataque.push_back(new Ataque("Sororidad", 30));
	acciones_ataque.push_back(new Ataque("Deconstruccion", 10));
	acciones_ataque.push_back(new Ataque("Consigna feminista", 25));
	acciones_ataque.push_back(new Ataque("Dato sobre violencia de genero", 15));
	acciones_ataque.push_back(new Ataque("Feminizar espacios masculinos", 15));

	acciones_defensa.push_back(new Ataque("Mansplaining", 20));
	acciones_defensa.push_back(new Ataque("Manspreading", 30));
	acciones_defensa.push_back(new Ataque("No escuchar", 5));
	acciones_defensa.push_back(new Ataque("Matizar su discurso", 10));
	acciones_defensa.push_back(new Ataque("Micromachismo", 15));
}

void NPC::newEntrepeneur()
{
	m_id = 0;

	nombre = "Entrepreneur";
	descripcion = "Hay que emprender";

	m_sprite = Sprite::create("Minijuego1/entrepreneur.png");
	m_sprite->retain();

	acciones_ataque.push_back(new Ataque("Dato: 75% PYMES fracasan", 30));
	acciones_ataque.push_back(new Ataque("Emprende humo", 10));
	acciones_ataque.push_back(new Ataque("Ladron de ideas", 25));
	acciones_ataque.push_back(new Ataque("La clave del exito es explotar a otros", 15));
	acciones_ataque.push_back(new Ataque("No creerte el discurso entrepreneur", 15));

	acciones_defensa.push_back(new Ataque("Explotar a lxs trabajadorxs", 20));
	acciones_defensa.push_back(new Ataque("Nadie triunfa sin fracasar antes", 30));
	acciones_defensa.push_back(new Ataque("Vender humo", 5));
	acciones_defensa.push_back(new Ataque("El precio del exito es trabajar duro", 10));
	acciones_defensa.push_back(new Ataque("No es una crisis, es una oportunidad", 15));
}

void NPC::newBurgues()
{
	m_id = 0;

	nombre = "Burgues";
	descripcion = "Burgues propietario de los medios de produccion";

	m_sprite = Sprite::create("Minijuego1/burgues.png");
	m_sprite->retain();

	acciones_ataque.push_back(new Ataque("Empezar una huelga", 30));
	acciones_ataque.push_back(new Ataque("Apuntarse al Partido Comunista", 10));
	acciones_ataque.push_back(new Ataque("Organizar a los trabajadores", 25));
	acciones_ataque.push_back(new Ataque("Tomar el poder", 15));
	acciones_ataque.push_back(new Ataque("Eliminar las clases sociales", 15));

	acciones_defensa.push_back(new Ataque("Explotar a lxs trabajadorxs", 20));
	acciones_defensa.push_back(new Ataque("Manipular elecciones a beneficio propio", 30));
	acciones_defensa.push_back(new Ataque("Recurrir a la ultraderecha cuando hay peligro economico", 5));
	acciones_defensa.push_back(new Ataque("Privatizar sanidad y educacion", 10));
	acciones_defensa.push_back(new Ataque("Iniciar una guerra para beneficios economicos", 15));
}

void NPC::newAntivacunas()
{
	m_id = 0;

	nombre = "Antivacunas";
	descripcion = "Persona en contra de las vacunas por una conspiracion";

	m_sprite = Sprite::create("Minijuego1/antivacunas.png");
	m_sprite->retain();

	acciones_ataque.push_back(new Ataque("Vacuna para curar el antivacunismo", 30));
	acciones_ataque.push_back(new Ataque("V de vacuna, V de vida", 10));
	acciones_ataque.push_back(new Ataque("Pinchazo por la espalda", 25));
	acciones_ataque.push_back(new Ataque("Seleccion natural", 15));
	acciones_ataque.push_back(new Ataque("Estornudarle encima para que se contagie de algo", 15));

	acciones_defensa.push_back(new Ataque("Las empresas nos controlan con las vacunas", 20));
	acciones_defensa.push_back(new Ataque("Viva la homeopatia", 30));
	acciones_defensa.push_back(new Ataque("La proteccion natural es mejor, como en el Paleolitico", 5));
	acciones_defensa.push_back(new Ataque("Los medicos son unos fascistas", 10));
	acciones_defensa.push_back(new Ataque("Es que nadie va a pensar en los ninos", 15));
}

void NPC::newAntiEcologista()
{
	m_id = 0;

	nombre = "Antiecologista";
	descripcion = "Enemigo del ecologismo";

	m_sprite = Sprite::create("Minijuego1/antiecologista.png");
	m_sprite->retain();

	acciones_ataque.push_back(new Ataque("Informe sobre plaguicidas toxicos en la agricultura industrial", 30));
	acciones_ataque.push_back(new Ataque("33000 muertes prematuras en Espana anualmente", 10));
	acciones_ataque.push_back(new Ataque("Cada ano unos 8 millones de toneladas de plasticos acaban en el mar", 25));
	acciones_ataque.push_back(new Ataque("Dato de los microplasticos en el agua de la lluvia", 15));
	acciones_ataque.push_back(new Ataque("Hablar sobre el envio de basura a paises colonizados", 15));

	acciones_defensa.push_back(new Ataque("Si la temperatura global sube, tendremos mas verano", 20));
	acciones_defensa.push_back(new Ataque("Donde esta la contaminacion, a ver que yo la vea", 30));
	acciones_defensa.push_back(new Ataque("A tope con el plastico", 5));
	acciones_defensa.push_back(new Ataque("A favor de que las empresas sean responsables del 70% de la contaminacion", 10));
	acciones_defensa.push_back(new Ataque("Comer mucha carne", 15));
}

bool NPC::atacar(int dano_ataque)
{
	bool derrotado;

	derrotado = false;

	salud_moral -= dano_ataque;

	if (salud_moral <= 0)
	{
		salud_moral = 0;
		derrotado = true;

		return false;
	}
	else
	{
		derrotado = false;
		return true;
	}
}

Ataque *NPC::defenderse()
{
	int num_ataques_defensa;
	int numero_ataque;

	num_ataques_defensa = this->acciones_defensa.size();

	numero_ataque = rand() % num_ataques_defensa;

	return this->acciones_defensa.at(numero_ataque);
}

vector<Ataque*> NPC::getAccionesAtaque()
{
	return acciones_ataque;
}

int NPC::getSaludMoral()
{
	return this->salud_moral;
}

bool NPC::permitirHuir()
{
	return this->permitir_huir;
}

Sprite *NPC::getSprite()
{
	return this->m_sprite;
}

char *NPC::getNombre()
{
	return this->nombre;
}

int NPC::getCodigoColision()
{
	return m_codigo_colision;
}