//
//  Mundo.h
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#pragma once

#include "../Engine2D/GameEntity.h"
#include "../Engine2D/PhysicsGameEntity.h"
#include "../Engine2D/TiledMapHelper.h"
#include "../Engine2D/CocosDebugDraw.h"
#include "cocos2d.h"
#include "Letrero.h"
#include "Textos.h"

class Mundo: public PhysicsGameEntity{
    
public:
    bool init();
    ~Mundo();
    static Mundo* getInstance();
    static void destroyInstance();
    //Nombre del mapa
    const char *nombreMapa;
    
	void reestablecerMapaInicial();
    virtual void preloadResources();
    virtual Node* getNode();
    void update(float delta);
        
    int getTileGIDAtPosition(const Point &tileCoords);
    Point tileCoordForPosition(const Vec2& position);
    Rect tileRectForPosition(const Vec2& position);
    void deleteTileAt(const Point &tileCoords);
    Size getTileSize();
    Size getMapSize();
    
    TiledMapHelper *getTileMapHelper() { return m_tiledMapHelper; }
    
    //Vector<NPC*> getNPCs();

    // Acceso al mundo fisico
    b2World *getPhysicsWorld() { return m_world; }
   
    const char* nameMapaTag = "map_name";
    const int nameMApaValue = 1;
    
private:
    
	CREATE_FUNC(Mundo);

	static Mundo *m_instance;
    TiledMapHelper *m_tiledMapHelper;
    //Vector<NPC*> m_npcs;

    // Mundo fisico
    b2World *m_world;
    CocosDebugDraw *m_debugDraw;

	const char *m_nombre_mapa;
	int m_mapa;
	char *m_nombre_fichero_textos;

	Textos *m_textos;

	void string_copy(const char *from, char *to);

	void inicializarNPC();
	void inicializarLetreros();
	void inicializarVirus();
    void initPhysics();
};

