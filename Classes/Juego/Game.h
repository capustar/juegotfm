#ifndef __GAME_H__
#define __GAME_H__

#include "cocos2d.h"
#include "../Engine2D/SimpleGame.h"
#include "../Juego/PhysicsPlayer.h"
#include "Mundo.h"
#include "MensajeDialogo.h"
#include "../Engine2D/VirtualControls.h"

USING_NS_CC;

class Game : public SimpleGame
{
public:

    bool init();
	~Game();

    // implement the "static create()" method manually
    CREATE_FUNC(Game);
    
	void preloadResources();
	void start();
	
	void moverMapa();

	void updateEachFrame(float delta);

	void playerColisionaConLetreros();
	void playerColisionaConNPC();
	void playerColisionaConVirus();

private:
	Mundo *m_mundo;

	Node *m_scrollable;

	VirtualControls *m_input_virtual;

	PhysicsPlayer *m_player;

	MensajeDialogo *mensaje_dialogo;

	Sprite *m_fondo_menu;
	Menu *m_menu_pausa;
	ui::Button *m_boton_pausa;

	void establecerIdioma();

	void mostrarDialogo();

	void volverMenuPrincipal(cocos2d::Ref *sender);
	void seguirJugando(cocos2d::Ref *sender);
	void mostrarMenuPausa();
	void crearMenuPausa();
	void anadirBotonPausa();
};

#endif // __GAME_H__
