#include "GameOverScene.h"
#include "MainMenu.h"

USING_NS_CC;

Scene* GameOverScene::createScene()
{
	return GameOverScene::create();
}

bool GameOverScene::init()
{
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto label = Label::createWithTTF("GAME OVER", "fonts/Marker Felt.ttf", 24);

	label->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height - label->getContentSize().height));

	// add the label as a child to this layer
	this->addChild(label, 1);

	this->scheduleOnce(schedule_selector(GameOverScene::GoToMainMenuScene), DISPLAY_TIME_SPLASH_SCENE);

	return true;
}

void GameOverScene::GoToMainMenuScene(float dt)
{
	auto scene = MainMenu::createScene();

	//Director::getInstance( )->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
	Director::getInstance()->replaceScene(TransitionFade::create(TRANSITION_TIME, scene));
}