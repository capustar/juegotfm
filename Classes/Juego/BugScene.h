#ifndef __BUGSCENE_SCENE_H__
#define __BUGSCENE_SCENE_H__

#include "cocos2d.h"

class BugScene : public cocos2d::Layer
{
	public:
		// there's no 'id' in cpp, so we recommend returning the class instance pointer
		static cocos2d::Scene* createScene();

		// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
		virtual bool init();

		// implement the "static create()" method manually
		CREATE_FUNC(BugScene);

	private:
		cocos2d::Sprite* _conejo;
		int m_collision_bitmask_bug;
		cocos2d::Vector<cocos2d::Sprite*> m_bugs;

		void inicializarToque();
		void agregarBug(float dt);
		void inicializarFisica(cocos2d::Sprite* sprite, int collisionBitmask);

		void update(float dt);
};

#endif // __BUGSCENE_SCENE_H__