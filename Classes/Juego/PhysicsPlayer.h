#pragma once

#include "../Engine2D/PhysicsGameEntity.h"
#include "Letrero.h"
#include "NPC.h"
#include "Virus.h"

class PhysicsPlayer : public PhysicsGameEntity {
public:

	bool init();
	CREATE_FUNC(PhysicsPlayer);

	void preloadResources();
	Node* getNode();

	void setSaludMoral(int nueva_salud_moral);
	int getSaludMoral();

	const Vec2& getPosition() {
		return m_playerSprite->getPosition();
	};

	void setPosition(const Vec2& pos) {
		this->setTransform(pos, 0.0f);
	}

	virtual void update(float dt);

	void move(float horizontalAxis, float verticalAxis, float delta);

	bool colisionaConLetreros();
	bool colisionaConNPC();
	bool colisionaConVirus();
	

	Letrero *letrero_colisionado;
	NPC *npc_colisionado;
	Virus *virus_colisionado;

private:
	Sprite *m_playerSprite;
	Sprite *m_playerSprite_izquierda;
	Sprite *m_playerSprite_abajo;
	Sprite *m_playerSprite_arriba;

	int salud_moral;

	float m_horizontalAxis;
	float m_verticalAxis;

	Speed *m_action_andar_derecha, *m_action_andar_izquierda, *m_action_andar_arriba, *m_action_andar_abajo, *m_action;

	void actualizarAnimacion();

};

