#include "Virus.h"
#include "../Engine2D/Definitions.h"

Virus::Virus(int i)
{
	this->m_codigo_colision = CODIGO_COLISION_VIRUS;

	switch (i)
	{
	case 0:

		newResistenteAntibioticos();
		break;

	case 1:

		newInfluenza();
		break;

	case 2:

		newDepresion();
		break;

	case 3:

		newEbola();
		break;

	case 4:

		newPaludismo();
		break;

	default:

		newDepresion();
		break;
	}
}

Virus::~Virus()
{
	m_sprite->release();
}

void Virus::newResistenteAntibioticos()
{
	this->m_id = 0;
	this->m_nombre = "Bacteria resistente a los antibioticos";
	this->m_descripcion = "Muchas bacterias han evolucionado y se han hecho resistentes a los antibioticos. Esto no solo aumento la mortalidad por infecciones, sino que la resistencia a los antibioticos tambien fue un gran obstaculo para las cirugias y trasplante de organos.";
	this->m_sprite = Sprite::create("Virus/virus1.png");
	m_sprite->retain();
}

void Virus::newInfluenza()
{
	this->m_id = 0;
	this->m_nombre = "Virus de la influenza";
	this->m_descripcion = "El virus de la influenza cambia con mucha rapidez y es muy contagioso, por esa razon se convirtio en una enfermedad verdaderamente letal. En 1929 mato a 100 millones de personas, y con la facilidad de transporte que existia antes del colapso hizo que se propagase mas facilmente";
	this->m_sprite = Sprite::create("Virus/virus2.png");
	m_sprite->retain();
}

void Virus::newDepresion()
{
	this->m_id = 0;
	this->m_nombre = "Depresion";
	this->m_descripcion = "La depresion es un trastorno mental frecuente, que se caracteriza por la presencia de tristeza, perdida de interes o placer, sentimientos de culpa o falta de autoestima, trastornos del sueno o del apetito, sensacion de cansancio y falta de concentracion. Padeciendola en su dia casi un 10% de la poblacion mundial.";
	this->m_sprite = Sprite::create("Virus/virus3.png");
	m_sprite->retain();
}

void Virus::newEbola()
{
	this->m_id = 0;
	this->m_nombre = "Ebola";
	this->m_descripcion = "Los patogenos que causan el ebola, zika o SARS causan enfermedades graves que no tienen cura. Lo unico que se hacia en el pasado cuando surgia algun brote era aislarlo y tratar los sintomas. La falta de preparacion causo una pandemia.";
	this->m_sprite = Sprite::create("Virus/virus4.png");
	m_sprite->retain();
}

void Virus::newPaludismo()
{
	this->m_id = 0;
	this->m_nombre = "Paludismo";
	this->m_descripcion = "Anualmente se registraban mas de 200 millones de casos de paludismo en el mundo. Esta enfermedad transmitida por mosquitos causaba mas de 400000 defunciones. Alrededor del 90% de la mortalidad se registraba en el Africa subsahariana";
	this->m_sprite = Sprite::create("Virus/virus5.png");
	m_sprite->retain();
}

int Virus::getId()
{
	return this->m_id;
}

char* Virus::getNombre()
{
	return this->m_nombre;
}

char* Virus::getDescripcion()
{
	return this->m_descripcion;
}

int Virus::getCodigoColision()
{
	return this->m_codigo_colision;
}

Sprite *Virus::getSprite()
{
	return this->m_sprite;
}