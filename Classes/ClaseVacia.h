#ifndef __CLASEVACIA_H__
#define __CLASEVACIA_H__

#include "cocos2d.h"

class ClaseVacia : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(ClaseVacia);
};

#endif // __CLASEVACIA_H__
