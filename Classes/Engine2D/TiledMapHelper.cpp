//
//  TiledMapHelper.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 18/1/15.
//
//

#include "TiledMapHelper.h"

bool TiledMapHelper::init()
{    
    return true;
}


void TiledMapHelper::posicionarTilemap()
{
	Size visible_size = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	m_tiledmap->setPosition(origin.x, origin.y);
	//m_tiledmap->setPosition(Vec2(-visible_size.width / 2 + origin.x + 14, -visible_size.height / 2 + origin.y + 14));
}

void TiledMapHelper::loadTileMap(const char *url, const char *withCollisionLayer) {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	m_tiledmap = TMXTiledMap::create(url);
	m_collision = m_tiledmap->getLayer(withCollisionLayer);

	//m_tiledmap->setPosition(Vec2(origin.x, visibleSize.height / 2 + origin.y));
	posicionarTilemap();
}

void TiledMapHelper::loadTileMap(const char *url, const char *withCollisionLayer, const char *withMessageLayer) {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	m_tiledmap = TMXTiledMap::create(url);
	m_collision = m_tiledmap->getLayer(withCollisionLayer);

	m_mensajes = m_tiledmap->getLayer(withMessageLayer);

	//m_tiledmap->setPosition(Vec2(origin.x, visibleSize.height / 2 + origin.y));
	//##Revisar coordenadas
	posicionarTilemap();
}

void TiledMapHelper::loadTileMap(const char *url, const char *withCollisionLayer, const char *withColectableLayer, const char *withComdaLayer, const char *withFinalLayer){
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    m_tiledmap = TMXTiledMap::create(url);
    m_collision = m_tiledmap->getLayer(withCollisionLayer);
    m_colectables = m_tiledmap->getLayer(withColectableLayer);
    m_comida = m_tiledmap->getLayer(withComdaLayer);
    m_final = m_tiledmap->getLayer(withFinalLayer);
    
	//m_tiledmap->setPosition(Vec2(origin.x, 0 + origin.y - 62));
	posicionarTilemap();
}

Point TiledMapHelper::tileCoordForPosition(const Point& position)
{
    Size tileSize = m_tiledmap->getTileSize();
    auto tpos = m_tiledmap->getPosition();
    
    float totalHeight = m_tiledmap->getMapSize().height * tileSize.height;
    float x = floor((position.x-tpos.x) / tileSize.width);
    float y = floor((totalHeight - (position.y-tpos.y)) / tileSize.height);
    return Point(x, y);
}

Rect TiledMapHelper::rectForTileAt(const Point &tileCoords) {
    Size tileSize = m_tiledmap->getTileSize();
    auto tpos = m_tiledmap->getPosition();
    
    float totalHeight = m_tiledmap->getMapSize().height * tileSize.height;
    Point origin = Point((tileCoords.x-tpos.x) * tileSize.width, totalHeight -
                         (((tileCoords.y-tpos.y) + 1) * tileSize.height));
    return Rect(origin.x, origin.y,
                tileSize.width, tileSize.height);
}

int TiledMapHelper::getTileGIDAt(const Point &tileCoords){
    Size s = m_collision->getLayerSize();
    
    if(tileCoords.x>=0 && tileCoords.x<s.width && tileCoords.y>=0 && tileCoords.y<s.height)
        return m_collision->getTileGIDAt(tileCoords);
    else {
        CCLOG("Warning: Accesing pos %f,%f in tilemap (out of range)", tileCoords.x, tileCoords.y);
        return 0;
    }
}

void TiledMapHelper::deleteTileAt(const Point &tileCoords){
    m_collision->setTileGID(0, tileCoords);
}


int TiledMapHelper::getTileGIDAtPosition(const Point &pos){
    return getTileGIDAt(tileCoordForPosition(pos));
}

TMXTiledMap *TiledMapHelper::getTiledMap(){
    return m_tiledmap;
}

TMXLayer *TiledMapHelper::getCollisionLayer(){
    return m_collision;
}

TMXLayer *TiledMapHelper::getColectableLayer(){
    return m_colectables;
}

TMXLayer *TiledMapHelper::getComidaLayer(){
    return m_comida;
}

TMXLayer *TiledMapHelper::getFinalLayer(){
    return m_final;
}
