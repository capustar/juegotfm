//
//  VirtualControls.hpp
//  ProjectTime
//
//  Created by Miguel Angel Lozano Ortega on 9/2/16.
//
//

#ifndef VirtualControls_hpp
#define VirtualControls_hpp

#include <stdio.h>
#include "GameEntity.h"
#include "cocos2d.h"
USING_NS_CC;

#define kNUM_BUTTONS    1
#define kNUM_AXIS    2

enum Button {
    BUTTON_ACTION=0,
    BUTTON_LEFT=1,
    BUTTON_RIGHT=2
};

enum Axis {
    AXIS_HORIZONTAL=0,
    AXIS_VERTICAL=1
};


class VirtualControls: public GameEntity {
public:
    
    bool physic_pad_connected;

    bool init();
    
    virtual void preloadResources(){};
    virtual Node* getNode() {return NULL;};
    
    bool isButtonPressed(Button button);
    float getAxis(Axis axis);
    
    std::function<void(Button)> onButtonPressed;
    std::function<void(Button)> onButtonReleased;
    
    virtual void addKeyboardListeners(cocos2d::Node *node);
    virtual void addTouchListeners(cocos2d::Node *node);

	virtual void returnInitialPosition();
        
    CREATE_FUNC(VirtualControls);
    
protected:

    bool buttonState[kNUM_BUTTONS];
    float axisState[kNUM_AXIS];
};


#endif /* VirtualControls_hpp */
