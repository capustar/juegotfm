//
//  VirtualControls.cpp
//  ProjectTime
//
//  Created by Miguel Angel Lozano Ortega on 9/2/16.
//
//

#include "VirtualControls.h"



bool VirtualControls::init(){
    GameEntity::init();
    
    for(int i=0;i<kNUM_BUTTONS;i++) {
        buttonState[i] = false;
    }
    
    for(int i=0;i<kNUM_AXIS;i++) {
        axisState[i] = 0.0f;
    }

    physic_pad_connected = false;
    
    return true;
}

bool VirtualControls::isButtonPressed(Button button) {
    return buttonState[button];
}

float VirtualControls::getAxis(Axis axis) {
    return clampf(axisState[axis], -1.0, 1.0);
}

// Keyboard input support

void VirtualControls::addKeyboardListeners(cocos2d::Node *node) 
{

}

void VirtualControls::addTouchListeners(cocos2d::Node *node) 
{
    
}

void VirtualControls::returnInitialPosition()
{

}
