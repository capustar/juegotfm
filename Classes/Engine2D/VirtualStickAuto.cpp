//
//  VirtualStickAuto.cpp
//  ProjectTime
//
//  Created by Miguel Angel Lozano Ortega on 19/3/15.
//
//

#include "VirtualStickAuto.h"

bool VirtualStickAuto::init(){
    VirtualControls::init();

	m_velocidad_movimiento = 1.0f;
    
    return true;
}

void VirtualStickAuto::preloadResources(){
    
    //Cache de sprites
    auto spriteFrameCache = SpriteFrameCache::getInstance();
    
    //Si no estaba el spritesheet en la caché lo cargo
    if(!spriteFrameCache->getSpriteFrameByName("boton-direccion.png")) 
	{
        spriteFrameCache->addSpriteFramesWithFile("Mando/mando.plist");
    }
}

Node* VirtualStickAuto::getNode()
{
    if(m_node==NULL) 
	{
        Size visibleSize = Director::getInstance()->getVisibleSize();
        Vec2 visibleOrigin = Director::getInstance()->getVisibleOrigin();
		float porcentaje_posicion_joystick;
		float joystick_x, joystick_y;

		porcentaje_posicion_joystick = 0.15;
		joystick_x = visibleOrigin.x + porcentaje_posicion_joystick * visibleSize.width;
		joystick_y = visibleOrigin.y + porcentaje_posicion_joystick * 1.5 * visibleSize.height;
        
        m_stickLeftBase = Sprite::createWithSpriteFrameName("base-stick.png");
        m_stickLeftBase->setAnchorPoint(Vec2(0.5,0.5));
		m_stickLeftBase->setPosition(Vec2(joystick_x, joystick_y));
        
        m_stickLeft = Sprite::createWithSpriteFrameName("bola-stick.png");
        m_stickLeft->setAnchorPoint(Vec2(0.5,0.5));
		m_stickLeft->setPosition(Vec2(joystick_x, joystick_y));
        
        m_radioStick = m_stickLeftBase->getContentSize() * 0.5 - m_stickLeft->getContentSize() * 0.5;
        
        m_node= Node::create();
        m_node->addChild(m_stickLeftBase,0);
        m_node->addChild(m_stickLeft,1);
        m_node->setLocalZOrder(100);
    }
    
    return m_node;
}

void VirtualStickAuto::addTouchListeners(cocos2d::Node *node) {
    EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {
        
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        
        if(rect.containsPoint(locationInNode)) {
            buttonState[target->getTag()] = true;
            if(onButtonPressed) {
                onButtonPressed((Button)target->getTag());
            }
            target->setOpacity(255);
            return true;
        }
        
        return false;
    };
    
    listener->onTouchEnded = [=](Touch* touch, Event* event) {
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        target->setOpacity(127);
        buttonState[target->getTag()] = false;
        if(onButtonReleased) {
            onButtonReleased((Button)target->getTag());
        }
    };
    
    // Listener stick
    listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](Touch* touch, Event* event) {
        
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        m_centerStick = target->convertToNodeSpace(touch->getLocation());
        Size winSize = Director::getInstance()->getWinSize();
        
        m_stickLeftBase->setPosition(m_centerStick);
        m_stickLeftBase->setVisible(true);
		m_stickLeftBase->setOpacity(255);

        m_stickLeft->setPosition(m_centerStick);
        m_stickLeft->setVisible(true);
		m_stickLeft->setOpacity(255);
            
        return true;
    };
    
    listener->onTouchMoved = [=](Touch* touch, Event* event) {
        Point offset = touch->getLocation()-touch->getStartLocation();
        
        Point max(m_radioStick);
        Point min(Point::ZERO-m_radioStick);
        offset.clamp(min, max);
        
        axisState[Axis::AXIS_VERTICAL] = offset.y / max.y;
        axisState[Axis::AXIS_HORIZONTAL] = offset.x / max.x;
        
        m_stickLeft->setPosition(m_centerStick + offset);
    };
    
    listener->onTouchEnded = [=](Touch* touch, Event* event) {
		m_stickLeftBase->setOpacity(100);
		m_stickLeft->setOpacity(100);
		m_stickLeft->setPosition(m_centerStick);

        
        axisState[Axis::AXIS_VERTICAL] = 0;
        axisState[Axis::AXIS_HORIZONTAL] = 0;
    };
    
    node->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, m_node);
    
}

void VirtualStickAuto::addKeyboardListeners(cocos2d::Node *node)
{
	//Creo listeners del teclado
	auto listener = cocos2d::EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(VirtualStickAuto::onKeyPressed, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, node);

	listener = cocos2d::EventListenerKeyboard::create();
	listener->onKeyReleased = CC_CALLBACK_2(VirtualStickAuto::onKeyReleased, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, node);
}

void VirtualStickAuto::onKeyPressed(EventKeyboard::KeyCode keyCode, cocos2d::Event *event) 
{
	if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	{
		axisState[Axis::AXIS_HORIZONTAL] -= m_velocidad_movimiento;
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	{
		axisState[Axis::AXIS_HORIZONTAL] += m_velocidad_movimiento;
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_UP_ARROW)
	{
		axisState[Axis::AXIS_VERTICAL] += m_velocidad_movimiento;
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_DOWN_ARROW)
	{
		axisState[Axis::AXIS_VERTICAL] -= m_velocidad_movimiento;
	}
}

void VirtualStickAuto::onKeyReleased(EventKeyboard::KeyCode keyCode, cocos2d::Event *event) 
{
	/*
	if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	{
		onButtonReleased(Button::BUTTON_LEFT);
		axisState[Axis::AXIS_HORIZONTAL] += 1.0;
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	{
		onButtonReleased(Button::BUTTON_RIGHT);
		axisState[Axis::AXIS_HORIZONTAL] -= 1.0;
	}
	*/

	if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	{
		axisState[Axis::AXIS_HORIZONTAL] = 0.0f;
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	{
		axisState[Axis::AXIS_HORIZONTAL] = 0.0f;
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_UP_ARROW)
	{
		axisState[Axis::AXIS_VERTICAL] = 0.0f;
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_DOWN_ARROW)
	{
		axisState[Axis::AXIS_VERTICAL] = 0.0f;
	}
}

void VirtualStickAuto::returnInitialPosition()
{
	m_stickLeft->setPosition(m_stickLeftBase->getPosition());
	axisState[Axis::AXIS_HORIZONTAL] = 0.0f;
	axisState[Axis::AXIS_VERTICAL] = 0.0f;
	m_stickLeftBase->setOpacity(100);
	m_stickLeft->setOpacity(100);
}