#ifndef __DEFINITIONS_H__
#define __DEFINITIONS_H__

#define DISPLAY_TIME_SPLASH_SCENE 2
#define TRANSITION_TIME 0.5
#define PTM_RATIO 16

#define FUENTE_TEXTOS "fonts/Marker Felt.ttf"

#define MAPA_TAG "map_name"

#define IDIOMA_TAG "language_name"
#define ESPANOL "espanol"
#define INGLES "ingles"

#define CODIGO_COLISION_NPC 14
#define CODIGO_COLISION_LETRERO 15
#define CODIGO_COLISION_VIRUS 16


#endif // __DEFINITIONS_H__
