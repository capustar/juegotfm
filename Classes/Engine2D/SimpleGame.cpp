//
//  EScene.cpp
//  Test01
//
//  Created by Fidel Aznar on 13/1/15.
//
//

#include "SimpleGame.h"

bool SimpleGame::init(){
    
    //Super
    Scene::init();

    //Update
    this->schedule(schedule_selector(SimpleGame::updateEachFrame));

    return true;
}

void SimpleGame::addGameEntity(GameEntity *ge){
    m_gameEntities.pushBack(ge);
}


void SimpleGame::removeGameEntity(GameEntity *ge){
    m_gameEntities.eraseObject(ge);
}

void SimpleGame::updateEachGameEntityWithDelta(float delta){
    for (auto e: this->m_gameEntities){
        e->update(delta);
    }
}
void SimpleGame::preloadEachGameEntity(){
    for (auto e: this->m_gameEntities){
        e->preloadResources();
    }
}
void SimpleGame::addEachGameEntityNodeTo(Node *node){
    for (auto e: this->m_gameEntities){
        node->addChild(e->getNode());
    }
}


void SimpleGame::onKeyPressed(EventKeyboard::KeyCode keyCode, Event *event){};
void SimpleGame::onKeyReleased(EventKeyboard::KeyCode keyCode, Event *event){};
